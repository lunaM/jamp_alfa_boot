<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="<c:url value="/resources/css/style.css" />">
<title>LMS</title>
</head>
<body>
	<jsp:include page="menu.jsp" />
	<div class="alert alert-danger" style="display:none" id="alert">
  		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  		<strong>Error!</strong> <h5></h5>
	</div>
	<h1>Learning Management System</h1>
	<h3>List of mentors who mentors more than 2 mentees at this moment</h3>
	<table>
		<thead>
			<tr>
				<th>Mentor</th>
				<th>Amount of mentees</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${mentors}" var="pair" varStatus="status">
					<tr>
						<td>${pair[0] }</td>
						<td>${pair[1] }</td>
					</tr>
			</c:forEach>
		</tbody>
	</table>
	
	<h3>List of mentees who are currently without mentors in selected location</h3>
	<input type="text" id="location" placeholder="Location">
	<input type="button" id="locBtn" value="Search">
	<div id="resultMentees"></div>
	
	<h3>List of all mentees and duration of their overall mentorship</h3>
	<div id="resultMenteesTime"></div>
	
	<h3>Statistic of how many programs currently exist within each city and how many people are involved into them</h3>
	<table>
		<thead>
			<tr>
				<th>Location</th>
				<th>Amount of participants</th>
				<th>Amount of programs</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${partStat}" var="partStat" varStatus="status">
					<tr>
						<td>${partStat[2] }</td>
						<td>${partStat[0] }</td>
						<td>${partStat[1] }</td>
					</tr>
			</c:forEach>
		</tbody>
	</table>	
		
	<h3>Statistic of success completions per a period</h3>
	From <input type="date" id="scsStart">
	Till <input type="date" id="scsEnd">
	<input type="button" id="scsBtn" value="Find">
	<table>
		<thead>
			<tr>
				<th>Phase</th>
				<th>Program</th>
				<th>Amount</th>
			</tr>
		</thead>
		<tbody id="completedStat">
		</tbody>
	</table>
	
	<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		function getMenteesOverallTime(start, amount){
			var li, ul = document.createElement('ul');
			$.ajax({
				type : 'POST',
				url : '/lms/getMenteesOverallTime',
				data : {
					start : start,
					amount : amount
				},
				complete : function(resp) {
					if (resp.responseJSON) {
						console.log(resp.responseJSON);
						resp.responseJSON.forEach(function(item){
							li = document.createElement('li');
							li.innerText = item[0]+' - '+Math.floor(item[1]/7)+' weeks';
							ul.appendChild(li);
						});
						let result = document.getElementById('resultMenteesTime');
						result.innerHTML = '';
						result.appendChild(ul);
					} else {
						document.getElementById('alert').style.display = 'inherit';
					}
	
				}
			});
		}
		window.onload = function() {
			getMenteesOverallTime(0,50);
			document.getElementById('locBtn').addEventListener('click', function() {
				var loc = document.getElementById('location').value;
				var li, ul = document.createElement('ul');
				if(!loc){
					document.getElementById('alert').style.display = 'inherit';
					return;
				}
				$.ajax({
					type : 'POST',
					url : '/lms/getMenteesWithoutMentors',
					data : {
						location : loc
					},
					complete : function(resp) {
						if (resp.responseJSON) {
							console.log(resp.responseJSON);
							resp.responseJSON.forEach(function(item){
								li = document.createElement('li');
								li.innerText = item[2];
								ul.appendChild(li);
							});
							let result = document.getElementById('resultMentees')
							result.innerHTML = '';
							result.appendChild(ul);
						} else {
							document.getElementById('alert').style.display = 'inherit';
						}
		
					}
				});
			});
			document.getElementById('scsBtn').addEventListener('click', function() {
				var start = document.getElementById('scsStart').value;
				var end = document.getElementById('scsEnd').value;
				var td, tr;
				if(!start || !end){
					document.getElementById('alert').style.display = 'inherit';
					return;
				}
				$.ajax({
					type : 'POST',
					url : '/lms/getCompletedParticipants',
					data : {
						start : start,
						end : end
					},
					complete : function(resp) {
						if (resp.responseJSON) {
							var table = document.getElementById('completedStat');
							console.log(resp.responseJSON);
							table.innerHTML = '';
							resp.responseJSON.forEach(function(item){
								tr = document.createElement('tr');
								for(var i=2; i>=0; i--){
									td = document.createElement('td');
									td.innerText = item[i];
									tr.appendChild(td);
								}
								table.appendChild(tr);
							});
						} else {
							document.getElementById('alert').style.display = 'inherit';
						}
		
					}
				});
			});
		}
	</script>
</body>
</html>