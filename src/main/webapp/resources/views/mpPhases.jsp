<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="<c:url value="/resources/css/style.css" />">
<title>LMS</title>
</head>
<body>
	<jsp:include page="menu.jsp" />
	<h1>Learning Management System</h1>
	<input type="hidden" id="mpId" value="${mpId }">
	<h2>Mentorship program phases </h3>
	<c:forEach items="${mpPhases}" var="mp"> 
	  	<ul>
		    <li>
		    	<h3>
		    	<a href="/lms/${mp.mp.id }/${mp.id}" >${mp.name}</a>
		    	</h3>
		    </li>
	    </ul>
	</c:forEach>
	<h3>Add new phase</h3>
	<input type="text" id="mpPhaseName" placeholder="Phase name">
	<button id="createNewPhase">Create</button>
	<div class="alert alert-danger" style="display:none" id="alert">
  		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  		<strong>Error!</strong> Unable to add. Invalid input values.
	</div>
	<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		window.onload = function(){
			document.getElementById('createNewPhase').addEventListener('click',
					function(){
						var name = document.getElementById('mpPhaseName').value;
						var mpId = document.getElementById('mpId').value;
						if(!name){
							alert('All fields are required');
							return;
						}
						$.ajax({
							type: 'POST',
					        url: '/lms/addMProgPhase',
					        data: {
					        	name : name,
					        	mpId : mpId
					        },
					        complete: function (resp) {
					        	console.log(resp);
					        	if(resp.status == 200){
					        		console.log(resp.responseJSON);
					        		location.reload();
					        	}else{
					        		document.getElementById('alert').style.display = 'inherit';
					        	}
					        	
					        }
						});
				});
		}

	</script>
</body>
</html>