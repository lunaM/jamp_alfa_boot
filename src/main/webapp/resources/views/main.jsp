<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="<c:url value="/resources/css/style.css" />">
<link rel="stylesheet" href="<c:url value="/resources/css/font-awesome.min.css" />">
<title>LMS</title>
</head>
<body>
	<jsp:include page="menu.jsp" />
	<h1>Learning Management System</h1>
	<h2>Mentorship programs</h2>
	<c:forEach items="${mentorshipPrograms}" var="mp"> 
	  	<ul>
		    <li>
		    	<h3>
		    	<a href = "/lms/${mp.id}/" >${mp.name}</a>
		    	<i class="fa fa-pencil-square-o" aria-hidden="true" id="${mp.id}" onclick="edit(this.id)"></i>
		    	</h3>
		    	<table>
		    		<tbody>
		    			<tr>
		    				<td>Office location</td>
		    				<td id="${mp.id}location">${mp.officeLocation}</td>
		    			</tr>
		    			<tr>
		    				<td>Start date</td>
		    				<td id="${mp.id}start">${mp.start}</td>
		    			</tr>
		    			<tr>
		    				<td>End date</td>
		    				<td id="${mp.id}end">${mp.end}</td>
		    			</tr>
		    		</tbody>
		    	</table>
		    </li>
	    </ul>
	</c:forEach>
	<h3>Add new mentorship program</h3>
	<jsp:include page="/resources/views/editMPform.jsp" ></jsp:include>
	
	<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/manageMP.js" />"></script>
	<script type="text/javascript">
		function edit(id){
			document.location.href = '/lms/editMentorshipProgram/'+id+'/';
		}
	</script>
</body>
</html>