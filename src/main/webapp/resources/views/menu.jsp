<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 
<div>
 
  <a href="${pageContext.request.contextPath}/lms/">Home</a>
 
  | &nbsp;
  
   <a href="${pageContext.request.contextPath}/admin">Admin</a>
  
  <c:if test="${pageContext.request.userPrincipal.name != null}">
  
     | &nbsp;
     <a href="${pageContext.request.contextPath}/logout">Logout</a>
     
  </c:if>
  
</div>