package com.epam.jamp.configurations;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@PropertySource("classpath:mail.properties")
@Configuration
public class MailingConfiguration {
	
	@Value("${mail.host}")
	private String host;
	@Value("${mail.port}")
	private int port;
	@Value("${mail.username}")
	private String username;
	@Value("${mail.password}")
	private String password;
	@Value("${mail.properties.transport.protocol}")
	private String transportProtocol;
	@Value("${mail.properties.smtp.auth}")
	private String smtpAuth;
	@Value("${mail.properties.smtp.starttls.enable}")
	private String smtpStarttlsEnable;
	private static final Logger logger = Logger.getLogger(MailingConfiguration.class);
	
	@Bean
	public JavaMailSenderImpl javaMailSenderImpl() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost(host);
		mailSender.setPort(port);
		mailSender.setUsername(username);
		mailSender.setPassword(password);
		Properties javaMailProperties = new Properties();
		javaMailProperties.setProperty("mail.transport.protocol", transportProtocol);
		javaMailProperties.setProperty("mail.smtp.auth", smtpAuth);
		javaMailProperties.setProperty("mail.smtp.starttls.enable", smtpStarttlsEnable);
		mailSender.setJavaMailProperties(javaMailProperties);
		logger.info("Created JavaMailSenderImpl");
		return mailSender;
	}
	
}
