package com.epam.jamp.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

@Configuration
// @EnableWebSecurity = @EnableWebMVCSecurity + Extra features
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	UserDetailsService authenticationService;
	@Autowired @Qualifier("customAuthenticationFailureHandler")
	AuthenticationFailureHandler customAuthenticationFailureHandler;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(authenticationService);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();
		http.authorizeRequests().antMatchers("/lms/**","/").hasAuthority("USER");
		http.authorizeRequests().antMatchers("/lms/**","/").hasAuthority("ADMINISTRATOR");
		//http.authorizeRequests().antMatchers("/admin").access("hasAnyRole('ADMINISTRATOR')");
		http.authorizeRequests().and().exceptionHandling().accessDeniedPage("/403");
		// configs for login form
		http.authorizeRequests().and().formLogin().loginProcessingUrl("/check").loginPage("/login")
				.defaultSuccessUrl("/lms/").failureHandler(customAuthenticationFailureHandler).usernameParameter("username")
				.passwordParameter("password").and().logout().logoutUrl("/logoutUser")
				.logoutSuccessUrl("/logoutSuccessful");
	}
}
