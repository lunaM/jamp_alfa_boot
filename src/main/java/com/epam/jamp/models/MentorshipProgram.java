package com.epam.jamp.models;

import java.util.Date;

import javax.persistence.*;

@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "name", "office_location" }))
@Entity
public class MentorshipProgram implements WatchedModel {

	@Id
	@GeneratedValue
	private Long id;
	private String name;
	@Column(name = "office_location")
	private String officeLocation;
	private Date start;
	private Date end;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "last_modified")
	private Date lastModified;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "modified_by")
	private String modifiedBy;

	public MentorshipProgram() {
	}

	public MentorshipProgram(String name, String officeLocation, Date start, Date end) {
		this.name = name;
		this.officeLocation = officeLocation;
		this.start = start;
		this.end = end;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOfficeLocation() {
		return officeLocation;
	}

	public void setOfficeLocation(String officeLocation) {
		this.officeLocation = officeLocation;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}
}
