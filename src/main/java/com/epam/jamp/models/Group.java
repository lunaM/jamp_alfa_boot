package com.epam.jamp.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "phase_group")
public class Group implements WatchedModel {
	@Id
	private Long id;
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "group_id", referencedColumnName = "id")
	private List<Pair> pair;
	@Column(name = "planned_start")
	private Date plannedStart;
	@Column(name = "planned_end")
	private Date plannedEnd;
	@Column(name = "actual_start")
	private Date actualStart;
	@Column(name = "actual_end")
	private Date actualEnd;
	@Enumerated(EnumType.ORDINAL)
	private GroupStatus status;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "last_modified")
	private Date lastModified;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "modified_by")
	private String modifiedBy;

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Pair> getPair() {
		return pair;
	}

	public void setPair(List<Pair> pair) {
		this.pair = pair;
	}

	public Group() {
		pair = new ArrayList<>();
	}

	public Date getPlannedStart() {
		return plannedStart;
	}

	public void setPlannedStart(Date plannedStart) {
		this.plannedStart = plannedStart;
	}

	public Date getPlannedEnd() {
		return plannedEnd;
	}

	public void setPlannedEnd(Date plannedEnd) {
		this.plannedEnd = plannedEnd;
	}

	public Date getActualStart() {
		return actualStart;
	}

	public void setActualStart(Date actualStart) {
		this.actualStart = actualStart;
	}

	public Date getActualEnd() {
		return actualEnd;
	}

	public void setActualEnd(Date actualEnd) {
		this.actualEnd = actualEnd;
	}

	public GroupStatus getStatus() {
		return status;
	}

	public void setStatus(GroupStatus status) {
		this.status = status;
	}

	public void addMentee(String mentee, String mentor) {
		pair.add(new Pair(mentee, mentor));
	}

	public void removeMentee(String mentee, String mentor) {
		Iterator<Pair> iterator = pair.iterator();
		Pair pair;
		while (iterator.hasNext()) {
			pair = iterator.next();
			if (pair.getMentee().equals(mentee) && pair.getMentor().equals(mentor)) {
				iterator.remove();
				return;
			}
		}
	}
}
