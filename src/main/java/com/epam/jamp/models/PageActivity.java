package com.epam.jamp.models;

import java.io.Serializable;
import java.util.Date;

public class PageActivity implements Serializable{

	private static final long serialVersionUID = 3643379507665114571L;
	private String activityType;
	private String description;
	private Date time;
	private String userId;
	
	public PageActivity() {}
	
	public PageActivity(String activityType, String description, Date time, String userId) {
		this.activityType = activityType;
		this.description = description;
		this.time = time;
		this.userId = userId;
	}
	
	public String getActivityType() {
		return activityType;
	}
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
