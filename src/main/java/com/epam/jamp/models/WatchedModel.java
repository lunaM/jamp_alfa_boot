package com.epam.jamp.models;

import java.util.Date;

public interface WatchedModel {
	
	Date getCreatedDate();

	void setCreatedDate(Date createdDate);

	Date getLastModified();

	void setLastModified(Date lastModified);

	String getCreatedBy();

	void setCreatedBy(String createdBy);

	String getModifiedBy();

	void setModifiedBy(String modifiedBy);
}
