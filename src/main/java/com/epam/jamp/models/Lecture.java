package com.epam.jamp.models;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.Future;

@Entity
public class Lecture {

	@Id
	@GeneratedValue
	private Long id;
	@Column(name = "domain_area")
	private String domainArea;
	private String topic;
	@OneToOne
	@JoinColumn(name = "lector_id")
	private Participant lector;
	private Long duration;
	@Future
	@Column(name = "sheduled_time")
	private Date sheduledTime;
	@ManyToOne
	@JoinColumn(name = "phase_id")
	private Phase phase;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Lecture() {
	}

	public Lecture(String domainArea, String topic, Long duration, Date sheduledTime) {
		this.domainArea = domainArea;
		this.topic = topic;
		this.duration = duration;
		this.sheduledTime = sheduledTime;
	}

	public String getDomainArea() {
		return domainArea;
	}

	public void setDomainArea(String domainArea) {
		this.domainArea = domainArea;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public Participant getLector() {
		return lector;
	}

	public void setLector(Participant lector) {
		this.lector = lector;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Date getSheduledTime() {
		return sheduledTime;
	}

	public void setSheduledTime(Date sheduledTime) {
		this.sheduledTime = sheduledTime;
	}

	@Override
	public String toString() {
		return "Lecture [domainArea=" + domainArea + ", topic=" + topic + ", lectorId=" + lector + ", duration="
				+ duration + ", sheduledTime=" + sheduledTime + "]";
	}

}
