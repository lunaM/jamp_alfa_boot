package com.epam.jamp.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
public class Phase implements WatchedModel {
	
	@Id
	@GeneratedValue
	private Long id;
	private String name;
	@ManyToOne
	@JoinColumn(name="mp_id")
	private MentorshipProgram mp;
	@OneToMany(targetEntity=Participant.class,mappedBy="phase" , fetch=FetchType.LAZY)
	private List<Participant> participants;
	@OneToOne
	@JoinColumn(name="group_id")
	private Group group;
	@OneToMany(targetEntity=Lecture.class,mappedBy="phase", fetch=FetchType.LAZY)
	private List<Lecture> lectures;
	@Column(name="created_date")
	private Date createdDate;
	@Column(name="last_modified")
	private Date lastModified;
	@Column(name="created_by")
	private String createdBy;
	@Column(name="modified_by")
	private String modifiedBy;

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Phase() {
		participants = new ArrayList<>();
		lectures = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Participant> getParticipants() {
		return participants;
	}

	public void setParticipants(List<Participant> participants) {
		this.participants = participants;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group groups) {
		this.group = groups;
	}

	public List<Lecture> getLectures() {
		return lectures;
	}

	public void setLectures(List<Lecture> lectures) {
		this.lectures = lectures;
	}

	@Override
	public String toString() {
		return "Phase [id=" + id + ", mpId=" + mp.getId() + ", participants=" + participants + ", groups=" + group
				+ ", lectures=" + lectures + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MentorshipProgram getMp() {
		return mp;
	}

	public void setMp(MentorshipProgram mp) {
		this.mp = mp;
	}

}
