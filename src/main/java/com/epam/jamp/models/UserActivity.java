package com.epam.jamp.models;

import java.io.Serializable;
import java.util.Date;

public class UserActivity implements Serializable, Similar<UserActivity> {

	private static final long serialVersionUID = 1L;
	private UserActivityType userActivity;
	private Date time;
	private boolean success;
	private String userId;
	private Long duration;

	public UserActivityType getUserActivity() {
		return userActivity;
	}

	public UserActivity(UserActivityType userActivity, Date time, boolean success, String userId, Long duration) {
		this.userActivity = userActivity;
		this.time = time;
		this.success = success;
		this.userId = userId;
		this.duration = duration;
	}

	public void setUserActivity(UserActivityType userActivity) {
		this.userActivity = userActivity;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	@Override
	public boolean isSimilar(UserActivity t) {
		if(t.getUserActivity().equals(getUserActivity()) && t.getUserId().equals(getUserId())){
			return true;
		}
		return false;
	}

}
