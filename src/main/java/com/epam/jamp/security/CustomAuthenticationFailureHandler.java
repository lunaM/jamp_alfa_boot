package com.epam.jamp.security;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.epam.jamp.models.UserActivity;
import com.epam.jamp.models.UserActivityType;
import com.epam.jamp.utils.UserActivityCacheManager;

@Component
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
	@Autowired
	private UserActivityCacheManager userActivityCacheManager;
	private static final String ADMIN_MAIL = "vik100@meta.ua";

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		super.onAuthenticationFailure(request, response, exception);
		String username = request.getParameter("username");
		userActivityCacheManager.setTimeToLive(TimeUnit.MINUTES.toMillis(10));
		userActivityCacheManager.checkSaveAndNotificate(
				new UserActivity(UserActivityType.LOGIN, new Date(), false, username, new Long(0)), 3, ADMIN_MAIL);
	}

	@Value(value = "/login?error=true")
	@Override
	public void setDefaultFailureUrl(String defaultFailureUrl) {
		super.setDefaultFailureUrl(defaultFailureUrl);
	}
}
