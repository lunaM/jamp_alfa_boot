package com.epam.jamp.aop;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoggingAspect {

	private static final Logger logger = Logger.getLogger(LoggingAspect.class);

	@Before("within(com.epam.jamp..*)")
	public void logDaoBefore(JoinPoint jp) throws Throwable {
		logger.info("Before executing method " + jp.getSignature().toShortString());
	}

	@AfterReturning("within(com.epam.jamp..*)")
	public void logDaoAfterReturning(JoinPoint jp) throws Throwable {
		logger.info("After executing method " + jp.getSignature().toShortString());
	}

	@AfterThrowing(pointcut = "within(com.epam.jamp..*)", throwing = "e")
	public void logDaoAfterThrowing(JoinPoint jp, Throwable e) throws Throwable {
		logger.error("Method " + jp.getSignature().toShortString() + " throws exception " + e);
	}
}
