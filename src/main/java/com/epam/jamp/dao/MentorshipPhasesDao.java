package com.epam.jamp.dao;

import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;

import org.springframework.stereotype.Repository;

import com.epam.jamp.models.Group;
import com.epam.jamp.models.Lecture;
import com.epam.jamp.models.Pair;
import com.epam.jamp.models.Participant;
import com.epam.jamp.models.Phase;
import com.epam.jamp.models.Role;

@Repository
public class MentorshipPhasesDao {

	@PersistenceContext
	EntityManager entityManager;

	public List<Phase> getAllMProgPhases(Long mpId) {
		String jpql = "SELECT DISTINCT mpP FROM Phase mpP WHERE mpP.mp.id = :mpId";
		TypedQuery<Phase> q = entityManager.createQuery(jpql, Phase.class);
		q.setParameter("mpId", mpId);
		return q.getResultList();
	}

	public boolean isParticipant(Long phaseId, String email, Role role) {
		String jpql = "SELECT DISTINCT * FROM Participant WHERE phase_id = :phaseId AND email LIKE :email AND role = :role";
		Query q = entityManager.createNativeQuery(jpql);
		q.setParameter("phaseId", phaseId);
		q.setParameter("email", email);
		q.setParameter("role", role.ordinal());
		if (q.getResultList().isEmpty()) {
			return false;
		}
		return true;
	}

	public void addMProgPhase(String mpId, String name) {
		String jpql = "INSERT INTO Phase (mp_id,name) VALUES(?,?)";
		Query q = entityManager.createNativeQuery(jpql);
		q.setParameter(1, mpId);
		q.setParameter(2, name);
		q.executeUpdate();
	}

	public Phase getPhase(Long mppId) {
		String jpql = "SELECT DISTINCT mpP FROM Phase mpP WHERE mpP.id = :mppId";
		TypedQuery<Phase> q = entityManager.createQuery(jpql, Phase.class);
		q.setParameter("mppId", mppId);
		Phase mpPhase = null;
		try {
			mpPhase = q.getSingleResult();
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		return mpPhase;
	}
	
	public List<Participant> getPhaseParticipants(Long mpId){
		String jpql = "SELECT DISTINCT mpP FROM Participant mpP WHERE mpP.phase.id = :mpId";
		TypedQuery<Participant> q = entityManager.createQuery(jpql, Participant.class);
		q.setParameter("mpId", mpId);
		return q.getResultList();
	}
	
	public List<Lecture> getPhaseLectures(Long mpId){
		String jpql = "SELECT DISTINCT mpL FROM Lecture mpL WHERE mpL.phase.id = :mpId";
		TypedQuery<Lecture> q = entityManager.createQuery(jpql, Lecture.class);
		q.setParameter("mpId", mpId);
		return q.getResultList();
	}

	public void addParticipant(Participant part, Long phaseId) {
		String jpql = "INSERT INTO Participant (role,status, phase_id,email) VALUES(?,?,?,?)";
		Query q = entityManager.createNativeQuery(jpql);
		q.setParameter(1, part.getRole().ordinal());
		q.setParameter(2, part.getStatus().ordinal());
		q.setParameter(3, phaseId);
		q.setParameter(4, part.getPerson().getEmail());
		q.executeUpdate();
	}

	public void updateParticipant(Participant part, Long phaseId) {
		String jpql = "UPDATE Participant SET  role=?, status=? WHERE id = ?";
		Query q = entityManager.createNativeQuery(jpql);
		q.setParameter(1, part.getRole().ordinal());
		q.setParameter(2, part.getStatus().ordinal());
		q.setParameter(3, part.getId());
		q.executeUpdate();
	}

	public void addLecture(Lecture lecture, Long phasId) {
		String jpql = "INSERT INTO Lecture (domain_area,duration, phase_id,sheduled_time, topic,lector_id) VALUES(?,?,?,?,?,?)";
		Query q = entityManager.createNativeQuery(jpql);
		q.setParameter(1, lecture.getDomainArea());
		q.setParameter(2, lecture.getDuration());
		q.setParameter(3, phasId);
		q.setParameter(4, lecture.getSheduledTime());
		q.setParameter(5, lecture.getTopic());
		q.setParameter(6, lecture.getLector().getId());
		q.executeUpdate();
	}

	public void addGroupToPhase(Group group, Long phaseId) {
		String jpql = "UPDATE Phase SET  group_id=? WHERE id=?";
		Query q = entityManager.createNativeQuery(jpql);
		q.setParameter(1, group.getId());
		q.setParameter(2, phaseId);
		q.executeUpdate();
	}

	public void saveGroup(Group group, Long phaseId) {
		entityManager.merge(group);
	}

	public void addGroupPair(Pair pair, Long groupId) {
		String jpql = "INSERT INTO Pair (mentee,mentor, group_id) VALUES(?,?,?)";
		Query q = entityManager.createNativeQuery(jpql);
		q.setParameter(1, pair.getMentee());
		q.setParameter(2, pair.getMentor());
		q.setParameter(3, groupId);
		q.executeUpdate();
	}

	public void removeGroupPair(Pair pair, Long groupId) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaDelete<Pair> delete = cb.createCriteriaDelete(Pair.class);
		Root<Pair> e = delete.from(Pair.class);
		Predicate[] predicates = new Predicate[3];
		predicates[0] = cb.equal(e.get("groupId"), groupId);
		predicates[1] = cb.equal(e.get("mentee"), pair.getMentee());
		predicates[2] = cb.equal(e.get("mentor"), pair.getMentor());
		delete.where(predicates);
		entityManager.createQuery(delete).executeUpdate();
	}

	public void setModifyPhaseDate(Long phaseId, String by) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaUpdate<Phase> update = cb.createCriteriaUpdate(Phase.class);
		Root<Phase> phase = update.from(Phase.class);
		update.set(phase.<Date> get("lastModified"), new Date());
		update.set(phase.<String> get("modifiedBy"), by);
		update.where(cb.equal(phase.get("id"), phaseId));
		entityManager.createQuery(update).executeUpdate();

	}

	public void setCreationPhaseDate(String mpId, String name, String by) {
		String jpql = "UPDATE Phase SET created_date=?, created_by=?  WHERE mp_id=? AND name LIKE ?";
		Query q = entityManager.createNativeQuery(jpql);
		q.setParameter(1, new Date());
		q.setParameter(2, by);
		q.setParameter(3, mpId);
		q.setParameter(4, name);
		q.executeUpdate();

	}
}