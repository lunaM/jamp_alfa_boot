package com.epam.jamp.controllers;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.security.core.context.SecurityContextHolder;

import com.epam.jamp.services.UserActivityService;
import com.epam.jamp.utils.UserActivityCacheManager;
import com.epam.jamp.models.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Controller
public class MainController {
	
	@Autowired
	private UserActivityService activityService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String main(Model model) {
		return "redirect:/lms/";
	}

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String adminPage(Model model) {
		return "adminPage";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage(Model model) {
		return "loginPage";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request, Model model, Principal principal) {
		activityService.sendUserActivity(new UserActivity(UserActivityType.LOGOUT, new Date(), true, principal.getName(),
				new Date().getTime() - request.getSession().getCreationTime()));
		return "redirect:/logoutUser";
	}
	
	@RequestMapping(value = "/logoutSuccessful", method = RequestMethod.GET)
	public String logoutSuccessfulPage(HttpServletRequest request, Model model, Principal principal) {
		model.addAttribute("title", "Logout");
		return "logoutSuccessfulPage";
	}

	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public String accessDenied(Model model, Principal principal) {

		if (principal != null) {
			model.addAttribute("message",
					"Hi " + principal.getName() + "<br> You do not have permission to access this page!");
		} else {
			model.addAttribute("msg", "You do not have permission to access this page!");
		}
		return "403Page";
	}
}
