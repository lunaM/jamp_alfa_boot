package com.epam.jamp.controllers;

import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.list.GrowthList;
import org.apache.commons.collections.list.LazyList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.epam.jamp.messaging.PageActivityMessageSender;
import com.epam.jamp.models.Lecture;
import com.epam.jamp.models.MentorshipProgram;
import com.epam.jamp.models.PageActivity;
import com.epam.jamp.models.Pair;
import com.epam.jamp.models.Participant;
import com.epam.jamp.models.ParticipantStatus;
import com.epam.jamp.models.People;
import com.epam.jamp.models.Phase;
import com.epam.jamp.models.Role;
import com.epam.jamp.services.AdditionalInfoService;
import com.epam.jamp.services.MentorshipPhasesService;
import com.epam.jamp.services.MentorshipProgramService;
import com.epam.jamp.services.PeopleService;

@Controller
@RequestMapping("/lms/*")
public class LmsController {
	@Autowired
	private PeopleService peopleService;
	@Autowired
	private AdditionalInfoService aiService;
	@Autowired
	private MentorshipProgramService mpService;
	@Autowired
	private MentorshipPhasesService mpPhasesService;
	@Autowired
	private PageActivityMessageSender pamSender;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		sdf.setLenient(true);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
	}

	@ExceptionHandler(DataAccessResourceFailureException.class)
	public ModelAndView handleDataAccessResourceFailureException(Exception ex) {
		ModelAndView model = new ModelAndView("errorPage");
		model.addObject("err", "Sorry! Can't connect to DB. \n" + ex);
		return model;

	}

	@ExceptionHandler(Exception.class)
	public ModelAndView handleAllException(Exception ex) {
		ModelAndView model = new ModelAndView("errorPage");
		model.addObject("err", "Sorry! An error occured.\n" + ex);
		return model;

	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String homepage(ModelMap model) {
		model.addAttribute("mentorshipPrograms", mpService.getAllMentorshipPrograms());
		return "main";
	}

	@RequestMapping(value = "/addMentorshipProgram", method = RequestMethod.POST)
	public @ResponseBody MentorshipProgram addMentorshipProgram(@RequestParam("id") String id,
			@RequestParam("name") String name, @RequestParam("officeLoc") String offLoc,
			@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate, ModelMap model,
			Principal principal) {
		Date start = null, end = null;
		try {
			start = sdf.parse(startDate);
			end = sdf.parse(endDate);
		} catch (ParseException e) {
			// log here
			e.printStackTrace();
		}
		if (start.compareTo(new Date()) < 1 || end.before(start)) {
			// log here
			System.err.println("Not correct date");
			return null;
		}
		MentorshipProgram mp = mpService.getMentorshipProgram(Long.valueOf(id));
		if (mp == null) {
			mp = new MentorshipProgram(name, offLoc, start, end);
		} else {
			mp.setName(name);
			mp.setOfficeLocation(offLoc);
			mp.setStart(start);
			mp.setEnd(end);
		}
		pamSender.sendMessage(new PageActivity("add/editMP", "Adding or editing mentorship program", new Date(),
				principal.getName()));
		mpService.addMentorshipProgram(mp);
		return mp;
	}

	@RequestMapping(value = "/{mpId}/", method = RequestMethod.GET)
	public String showMPPhase(@PathVariable("mpId") String mpId, ModelMap model) {
		model.addAttribute("mpPhases", mpPhasesService.getAllMProgPhases(Long.valueOf(mpId)));
		model.addAttribute("mpId", mpId);
		return "mpPhases";
	}

	@RequestMapping(value = "/{mpId}/{mppId}", method = RequestMethod.GET)
	public String showMPPhase(@PathVariable("mpId") String mpId, @PathVariable("mppId") String mppId, ModelMap model) {
		Phase phase = mpPhasesService.getPhase(Long.valueOf(mppId));
		if (phase == null) {
			return "error404";
		}
		phase.setLectures(mpPhasesService.getPhaseLectures(Long.valueOf(mpId)));
		phase.setParticipants(GrowthList.decorate(
				LazyList.decorate(mpPhasesService.getPhaseParticipants(phase.getId()), FactoryUtils.instantiateFactory(Participant.class))));
		model.addAttribute("phase", phase);
		return "mpPhaseDetail";
	}

	@RequestMapping(value = "/addMProgPhase", method = RequestMethod.POST)
	public ResponseEntity<String> addMPPhases(@RequestParam("mpId") String mpId, @RequestParam("name") String name,
			ModelMap model, Principal principal) {
		mpPhasesService.addMProgPhase(mpId, name);
		pamSender.sendMessage(
				new PageActivity("addMPPhase", "Adding new mentorship program phase", new Date(), principal.getName()));
		return new ResponseEntity<String>(HttpStatus.OK);
	}

	@RequestMapping(value = "/addParticipant", method = RequestMethod.POST)
	public ResponseEntity<String> addParticipant(@RequestParam("mppId") String mppId,
			@RequestParam("email") String email, @RequestParam("role") String role,
			@RequestParam("status") String status, ModelMap model, Principal principal) {
		People person = peopleService.getPersonById(email);
		if (person == null) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
		Participant part = new Participant(person, Role.valueOf(role), ParticipantStatus.valueOf(status));
		if (mpPhasesService.isParticipant(Long.valueOf(mppId), part.getPerson().getEmail(), part.getRole())) {
			return new ResponseEntity<String>(HttpStatus.CONFLICT);
		}
		mpPhasesService.addParticipant(part, Long.valueOf(mppId));
		pamSender.sendMessage(
				new PageActivity("addParticipant", "Adding new participant", new Date(), principal.getName()));
		return new ResponseEntity<String>(HttpStatus.OK);
	}

	@RequestMapping(value = "/addPerson", method = RequestMethod.GET)
	public String addPerson(ModelMap model) {
		model.addAttribute("people", peopleService.getAll());
		model.addAttribute("newPerson", new People());
		return "personPage";
	}

	@RequestMapping(value = "/addPerson", method = RequestMethod.POST)
	public String addPersonData(ModelMap model, @ModelAttribute("newPerson") @Valid People person, BindingResult result,
			Principal principal) {
		if (result.hasErrors()) {
			model.addAttribute("people", peopleService.getAll());
			return "personPage";
		}
		People peop = peopleService.getPersonById(person.getEmail());
		if (peop != null) {
			person.setCreatedBy(peop.getCreatedBy());
			person.setCreatedDate(peop.getCreatedDate());
			person.setModifiedBy(peop.getModifiedBy());
			person.setLastModified(peop.getLastModified());
		}
		peopleService.addPeople(person);
		model.addAttribute("people", peopleService.getAll());
		pamSender.sendMessage(
				new PageActivity("add/editPerson", "Adding or editing person info", new Date(), principal.getName()));
		return "redirect:/lms/addPerson";
	}

	@RequestMapping(value = "/saveParticipantsChanges", method = RequestMethod.POST)
	public String saveParticipantsChanges(ModelMap model, @ModelAttribute("phase") @Valid Phase phasePart,
			BindingResult result) {
		phasePart.getParticipants().forEach((item) -> {
			mpPhasesService.updateParticipant(item, phasePart.getId());
		});
		return "redirect:/lms/" + phasePart.getMp().getId() + "/" + phasePart.getId();
	}

	@RequestMapping(value = "/addLecture", method = RequestMethod.POST)
	public String addLecture(@RequestParam(value = "mpId") String mpId, @RequestParam("phaseId") String phaseId,
			@RequestParam("domainArea") String domainArea, @RequestParam("topic") String topic,
			@RequestParam("lectorId") String lectorId, @RequestParam("duration") String duration,
			@RequestParam("sheduledTime") Date sheduledTime, Principal principal) {
		Lecture lecture = new Lecture(domainArea, topic, Long.valueOf(duration), sheduledTime);
		People peop = peopleService.getPersonById(lectorId);
		if (peop == null) {
			System.err.println("Not found user");
			return "redirect:/lms/" + mpId + "/" + phaseId;
		}
		lecture.setLector(new Participant(peop, Role.LECTOR, ParticipantStatus.PROPOSED));
		mpPhasesService.addLecture(lecture, Long.valueOf(phaseId));
		pamSender.sendMessage(
				new PageActivity("add/editLecture", "Adding or editing lecture", new Date(), principal.getName()));
		return "redirect:/lms/" + mpId + "/" + phaseId;
	}

	@RequestMapping(value = "/removeMenteeFromGroup", method = RequestMethod.POST)
	public ResponseEntity<String> removeMenteeFromGroup(@RequestParam(value = "mppId") String mppId,
			@RequestParam("mentee") String mentee, @RequestParam("mentor") String mentor, Principal principal) {
		Phase phase = mpPhasesService.getPhase(Long.valueOf(mppId));
		mpPhasesService.removeGroupPair(new Pair(mentee, mentor), phase.getGroup().getId());
		pamSender.sendMessage(
				new PageActivity("removePair", "Removing mentor and mentee pair", new Date(), principal.getName()));
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/addMenteeToGroup", method = RequestMethod.POST)
	public ResponseEntity<String> addMenteeToGroup(@RequestParam(value = "mppId") String mppId,
			@RequestParam("mentee") String mentee, @RequestParam("mentor") String mentor, Principal principal) {
		if (!mpPhasesService.isParticipant(Long.valueOf(mppId), mentee, Role.MENTEE)
				|| !mpPhasesService.isParticipant(Long.valueOf(mppId), mentor, Role.MENTOR)) {
			return new ResponseEntity<String>(HttpStatus.FORBIDDEN);
		}
		Phase phase = mpPhasesService.getPhase(Long.valueOf(mppId));
		mpPhasesService.addGroupPair(new Pair(mentee, mentor), phase.getGroup().getId());
		pamSender.sendMessage(
				new PageActivity("addPair", "Adding mentor and mentee pair", new Date(), principal.getName()));
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/saveGroup", method = RequestMethod.POST)
	public String saveGroup(ModelMap model, @ModelAttribute("phase") @Valid Phase phasePart, BindingResult result,
			Principal principal) {
		phasePart.getGroup().setId(phasePart.getId());
		mpPhasesService.saveGroup(phasePart.getGroup(), phasePart.getId());
		mpPhasesService.addGroupToPhase(phasePart.getGroup(), phasePart.getId());
		pamSender.sendMessage(
				new PageActivity("saveGroup", "Saving mentorship program group changes", new Date(), principal.getName()));
		return "redirect:/lms/" + phasePart.getMp().getId() + "/" + phasePart.getId();
	}

	@RequestMapping(value = "/editMentorshipProgram/{mpId}/", method = RequestMethod.GET)
	public String editMentorshipProgram(ModelMap model, @PathVariable("mpId") String mpId) {
		model.addAttribute("mp", mpService.getMentorshipProgram(Long.valueOf(mpId)));
		return "editMP";
	}

	// Get list of mentors who mentors more than 2 mentees at this moment (only
	// active).
	@RequestMapping(value = "/additionalInfo", method = RequestMethod.GET)
	public String getAdditionalInfo(ModelMap model) {
		// Get list of mentors who mentors more than 2 mentees at this moment
		// (only active).
		List<Object[]> mentors = aiService.getMentorsByMenteesAmount(2);
		// Get statistic of how many programs currently exist within each city
		// and how many people are involved into them
		List<Object[]> participantStatistic = aiService.getParticipantStatistic();
		model.addAttribute("mentors", mentors);
		model.addAttribute("partStat", participantStatistic);
		return "additionalInfoPage";
	}

	// Get list of mentees who are currently without mentors in user-selected
	// location.
	@RequestMapping(value = "/getMenteesWithoutMentors", method = RequestMethod.POST)
	public @ResponseBody List<Participant> getMenteesWithoutMentors(@RequestParam("location") String location,
			ModelMap model) {
		// Get list of mentors who mentors more than 2 mentees at this moment
		// (only active).
		return aiService.getMenteesWithoutMentors(location);
	}

	// Get list of all mentees and duration of their overall mentorship (counted
	// in mentor-weeks) ordered descending with pagination.
	@RequestMapping(value = "/getMenteesOverallTime", method = RequestMethod.POST)
	public @ResponseBody List<Object[]> getMenteesOverallTime(@RequestParam("start") String start,
			@RequestParam("amount") String amount, ModelMap model) {
		return aiService.getMenteesOverallTime(Integer.valueOf(start), Integer.valueOf(amount));
	}

	// Get statistic of success completions per user-selected period of time.
	@RequestMapping(value = "/getCompletedParticipants", method = RequestMethod.POST)
	public @ResponseBody List<Object[]> getCompletedParticipants(@RequestParam("start") String start,
			@RequestParam("end") String end, ModelMap model) {
		return aiService.getCompletedParticipants(start, end);
	}
}
