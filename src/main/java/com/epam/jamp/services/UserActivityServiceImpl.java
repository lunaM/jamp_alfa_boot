package com.epam.jamp.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.jamp.messaging.UserActivityMessageSender;
import com.epam.jamp.models.UserActivity;

@Service("userActivityService")
public class UserActivityServiceImpl implements UserActivityService{
	static final Logger logger = LoggerFactory.getLogger(UserActivityServiceImpl.class);
	@Autowired
	UserActivityMessageSender messageSender;
	
	@Override
	public void sendUserActivity(UserActivity activity) {
		logger.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
		logger.info("Application : sending user activity", activity);
		messageSender.sendMessage(activity);
		logger.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
	}

}
