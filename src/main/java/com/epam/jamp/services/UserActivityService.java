package com.epam.jamp.services;

import com.epam.jamp.models.UserActivity;

public interface UserActivityService {
	public void sendUserActivity(UserActivity activity);
}
