package com.epam.jamp.services;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.epam.jamp.aop.ModifyPhase;
import com.epam.jamp.aop.ModifyModel;
import com.epam.jamp.aop.SaveToDB;
import com.epam.jamp.dao.MentorshipPhasesDao;
import com.epam.jamp.models.Group;
import com.epam.jamp.models.Lecture;
import com.epam.jamp.models.Pair;
import com.epam.jamp.models.Participant;
import com.epam.jamp.models.Phase;
import com.epam.jamp.models.Role;

@Component
public class MentorshipPhasesService {

	@Autowired
	private MentorshipPhasesDao mppDao;

	@Transactional
	public List<Phase> getAllMProgPhases(Long mpId) {
		return mppDao.getAllMProgPhases(mpId);
	}

	@Transactional
	public void addMProgPhase(String mpId, String name) {
		mppDao.addMProgPhase(mpId, name);
	}

	@Transactional
	public Phase getPhase(Long mppId) {
		return mppDao.getPhase(mppId);
	}

	@Transactional
	public boolean isParticipant(Long phaseId, String email, Role role) {
		return mppDao.isParticipant(phaseId, email, role);
	}

	@SaveToDB
	@ModifyModel
	@ModifyPhase
	@Transactional
	public void addParticipant(Participant part, Long phaseId) {
		mppDao.addParticipant(part, phaseId);
	}

	@ModifyPhase
	@Transactional
	public void updateParticipant(Participant part, Long phaseId) {
		System.out.println("~~~~~" + part);
		mppDao.updateParticipant(part, phaseId);
	}

	@ModifyModel
	@ModifyPhase
	@Transactional
	public void addLecture(Lecture lecture, Long phaseId) {
		mppDao.addLecture(lecture, phaseId);
	}

	@ModifyModel
	@ModifyPhase
	@Transactional
	public void saveGroup(Group group, Long phaseId) {
		mppDao.saveGroup(group, phaseId);
	}

	@ModifyModel
	@ModifyPhase
	@Transactional
	public void addGroupToPhase(Group group, Long phaseId) {
		mppDao.addGroupToPhase(group, phaseId);
	}

	@ModifyModel
	@ModifyPhase
	@Transactional
	public void addGroupPair(Pair pair, Long groupId) {
		mppDao.addGroupPair(pair, groupId);
	}

	@ModifyModel
	@ModifyPhase
	@Transactional
	public void removeGroupPair(Pair pair, Long groupId) {
		mppDao.removeGroupPair(pair, groupId);
	}

	@Transactional
	public void setModifyPhaseDate(Long phaseId, String by) {
		mppDao.setModifyPhaseDate(phaseId, by);
	}

	public void setCreationPhaseDate(String mpId, String name, String by) {
		mppDao.setCreationPhaseDate(mpId, name, by);
	}
	
	public List<Participant> getPhaseParticipants(Long mpId){
		return mppDao.getPhaseParticipants(mpId);
	}
	
	public List<Lecture> getPhaseLectures(Long mpId){
		return mppDao.getPhaseLectures(mpId);
	}
}