package com.epam.jamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;

@SpringBootApplication
public class WapAlfaBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(WapAlfaBootApplication.class, args);
	}
}
